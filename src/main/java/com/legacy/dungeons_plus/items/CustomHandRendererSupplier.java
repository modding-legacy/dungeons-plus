package com.legacy.dungeons_plus.items;

import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

public interface CustomHandRendererSupplier
{
	@OnlyIn(Dist.CLIENT)
	com.legacy.dungeons_plus.client.renderers.CustomHandRenderer getHandRenderer();
}
