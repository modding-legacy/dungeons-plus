package com.legacy.dungeons_plus.registry;

import com.legacy.dungeons_plus.DungeonsPlus;
import com.legacy.dungeons_plus.data.advancement.ThrownItemHitBlockTrigger;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.registrar.Registrar;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.advancements.CriterionTrigger;
import net.minecraft.core.registries.Registries;

@RegistrarHolder
public class DPCriteriaTriggers
{
	public static final RegistrarHandler<CriterionTrigger<?>> HANDLER = RegistrarHandler.getOrCreate(Registries.TRIGGER_TYPE, DungeonsPlus.MODID);

	public static final Registrar.Static<ThrownItemHitBlockTrigger> THROWN_ITEM_HIT_BLOCK = HANDLER.createStatic("thrown_item_hit_block", ThrownItemHitBlockTrigger::new);
}
