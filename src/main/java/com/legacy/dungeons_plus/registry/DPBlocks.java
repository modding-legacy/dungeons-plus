package com.legacy.dungeons_plus.registry;

import com.legacy.dungeons_plus.DungeonsPlus;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.registrar.Registrar;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockBehaviour;

@RegistrarHolder
public class DPBlocks
{
	public static final RegistrarHandler.BlockHandler HANDLER = RegistrarHandler.getOrCreateBlocks(DungeonsPlus.MODID);

	public static final Registrar.Static<Block> GRANITE_IRON_ORE = HANDLER.createStatic("granite_iron_ore", () -> new Block(BlockBehaviour.Properties.ofFullCopy(Blocks.IRON_ORE)), new Item.Properties());
	public static final Registrar.Static<Block> GRANITE_GOLD_ORE = HANDLER.createStatic("granite_gold_ore", () -> new Block(BlockBehaviour.Properties.ofFullCopy(Blocks.GOLD_ORE)), new Item.Properties());

}
