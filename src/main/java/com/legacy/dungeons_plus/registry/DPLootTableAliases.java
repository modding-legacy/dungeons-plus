package com.legacy.dungeons_plus.registry;

import static com.legacy.dungeons_plus.DungeonsPlus.*;

import com.legacy.dungeons_plus.DungeonsPlus;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.StructureGelRegistries;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.resources.ResourceLocation;
import net.neoforged.neoforge.registries.RegisterEvent.RegisterHelper;

@RegistrarHolder
public class DPLootTableAliases
{
	public static final RegistrarHandler<ResourceLocation> HANDLER = RegistrarHandler.getOrCreate(StructureGelRegistries.Keys.LOOT_TABLE_ALIAS, DungeonsPlus.MODID).addListener(DPLootTableAliases::register);
	
	protected static void register(RegisterHelper<ResourceLocation> event)
	{
		// Tower
		event.register(locate("tower/common"), DPLoot.Tower.CHEST_COMMON);
		event.register(locate("tower/barrel"), DPLoot.Tower.CHEST_BARREL);
		event.register(locate("tower/vex"), DPLoot.Tower.CHEST_VEX);
		event.register(locate("tower/vex_map"), DPLoot.Tower.CHEST_VEX_MAP);

		event.register(locate("tower/skeleton"), DPLoot.Tower.ENTITY_SKELETON);
		event.register(locate("tower/spider"), DPLoot.Tower.ENTITY_SPIDER);
		event.register(locate("tower/zombie"), DPLoot.Tower.ENTITY_ZOMBIE);

		// Reanimated ruins
		event.register(locate("reanimated_ruins/common"), DPLoot.ReanimatedRuins.CHEST_COMMON);
		event.register(locate("reanimated_ruins/desert"), DPLoot.ReanimatedRuins.CHEST_DESERT);
		event.register(locate("reanimated_ruins/desert_map"), DPLoot.ReanimatedRuins.CHEST_DESERT_MAP);
		event.register(locate("reanimated_ruins/frozen"), DPLoot.ReanimatedRuins.CHEST_FROZEN);
		event.register(locate("reanimated_ruins/frozen_map"), DPLoot.ReanimatedRuins.CHEST_FROZEN_MAP);
		event.register(locate("reanimated_ruins/mossy"), DPLoot.ReanimatedRuins.CHEST_MOSSY);
		event.register(locate("reanimated_ruins/mossy_map"), DPLoot.ReanimatedRuins.CHEST_MOSSY_MAP);

		event.register(locate("reanimated_ruins/skeleton"), DPLoot.ReanimatedRuins.ENTITY_SKELETON);
		event.register(locate("reanimated_ruins/zombie"), DPLoot.ReanimatedRuins.ENTITY_ZOMBIE);

		// Leviathan
		event.register(locate("leviathan/common"), DPLoot.Leviathan.CHEST_COMMON);
		event.register(locate("leviathan/rare"), DPLoot.Leviathan.CHEST_RARE);

		event.register(locate("leviathan/husk"), DPLoot.Leviathan.ENTITY_HUSK);

		// Snowy Temple
		event.register(locate("snowy_temple/common"), DPLoot.SnowyTemple.CHEST_COMMON);
		event.register(locate("snowy_temple/rare"), DPLoot.SnowyTemple.CHEST_RARE);

		event.register(locate("snowy_temple/stray"), DPLoot.SnowyTemple.ENTITY_STRAY);

		// Warped Garden
		event.register(locate("warped_garden/common"), DPLoot.WarpedGarden.CHEST_COMMON);
		event.register(locate("warped_garden/rare"), DPLoot.WarpedGarden.CHEST_RARE);

		// Soul Prison
		event.register(locate("soul_prison/common"), DPLoot.SoulPrison.CHEST_COMMON);
		event.register(locate("soul_prison/rare"), DPLoot.SoulPrison.CHEST_RARE);
		event.register(locate("soul_prison/golden_armor"), DPLoot.SoulPrison.CHEST_GOLDEN_ARMOR);
	}
}
