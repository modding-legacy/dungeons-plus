package com.legacy.dungeons_plus.registry;

import java.util.List;

import com.legacy.dungeons_plus.DungeonsPlus;
import com.legacy.dungeons_plus.data.providers.DPLootProv;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.registrar.Registrar;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;
import com.legacy.structure_gel.api.structure.processor.RandomBlockSwapProcessor;

import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.valueproviders.ConstantInt;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.levelgen.structure.templatesystem.AlwaysTrueTest;
import net.minecraft.world.level.levelgen.structure.templatesystem.BlockMatchTest;
import net.minecraft.world.level.levelgen.structure.templatesystem.CappedProcessor;
import net.minecraft.world.level.levelgen.structure.templatesystem.PosAlwaysTrueTest;
import net.minecraft.world.level.levelgen.structure.templatesystem.ProcessorRule;
import net.minecraft.world.level.levelgen.structure.templatesystem.RuleProcessor;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureProcessor;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureProcessorList;
import net.minecraft.world.level.levelgen.structure.templatesystem.rule.blockentity.AppendLoot;

@RegistrarHolder
public class DPProcessors
{
	public static final RegistrarHandler<StructureProcessorList> HANDLER = RegistrarHandler.getOrCreate(Registries.PROCESSOR_LIST, DungeonsPlus.MODID);

	public static final Registrar.Pointer<StructureProcessorList> END_RUINS_TOWER = HANDLER.createPointer("end_ruins_tower", () -> listOf(new RandomBlockSwapProcessor(Blocks.END_STONE_BRICKS, 0.1F, Blocks.END_STONE)));
	public static final Registrar.Pointer<StructureProcessorList> LEVIATHAN_ARCHAEOLOGY = HANDLER.createPointer("leviathan_archaeology", bootstrap ->
	{
		return new StructureProcessorList(List.of(archyProcessor(DPLootProv.LEVIATHAN_ARCHAEOLOGY_COMMON, 3), archyProcessor(DPLootProv.LEVIATHAN_ARCHAEOLOGY_RARE, 1)));
	});
	public static final Registrar.Pointer<StructureProcessorList> LEVIATHAN_ARCHAEOLOGY_MANY = HANDLER.createPointer("leviathan_archaeology_many", bootstrap ->
	{
		return new StructureProcessorList(List.of(archyProcessor(DPLootProv.LEVIATHAN_ARCHAEOLOGY_COMMON, 8), archyProcessor(DPLootProv.LEVIATHAN_ARCHAEOLOGY_RARE, 1)));
	});

	private static StructureProcessorList listOf(StructureProcessor... processors)
	{
		return new StructureProcessorList(List.of(processors));
	}

	private static CappedProcessor archyProcessor(ResourceLocation lootTable, int limit)
	{
		return new CappedProcessor(new RuleProcessor(List.of(new ProcessorRule(new BlockMatchTest(Blocks.SAND), AlwaysTrueTest.INSTANCE, PosAlwaysTrueTest.INSTANCE, Blocks.SUSPICIOUS_SAND.defaultBlockState(), new AppendLoot(lootTable)))), ConstantInt.of(limit));
	}
}
