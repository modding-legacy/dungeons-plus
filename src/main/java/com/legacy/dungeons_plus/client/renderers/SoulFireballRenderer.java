package com.legacy.dungeons_plus.client.renderers;

import org.joml.Matrix3f;
import org.joml.Matrix4f;
import org.joml.Quaternionf;

import com.legacy.dungeons_plus.DungeonsPlus;
import com.legacy.dungeons_plus.entities.SoulFireballEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Axis;

import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.Sheets;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.texture.TextureAtlas;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.resources.model.Material;
import net.minecraft.client.resources.model.ModelBakery;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.inventory.InventoryMenu;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class SoulFireballRenderer extends EntityRenderer<SoulFireballEntity>
{
	private static final ResourceLocation TEXTURE_LOCATION = DungeonsPlus.locate("textures/entity/soul_fireball.png");
	private static final RenderType RENDER_TYPE = RenderType.entityCutoutNoCull(TEXTURE_LOCATION);

	public SoulFireballRenderer(EntityRendererProvider.Context context)
	{
		super(context);
	}

	@Override
	protected int getBlockLightLevel(SoulFireballEntity entity, BlockPos pos)
	{
		return 12;
	}

	@Override
	public void render(SoulFireballEntity entity, float p_114081_, float partialTick, PoseStack poseStack, MultiBufferSource buffSource, int packedLighting)
	{
		poseStack.pushPose();
		float scale = 1.0F;
		poseStack.scale(scale, scale, scale);
		poseStack.mulPose(this.entityRenderDispatcher.cameraOrientation());
		poseStack.mulPose(Axis.YP.rotationDegrees(180.0F));
		PoseStack.Pose lastPose = poseStack.last();
		Matrix4f matrix4f = lastPose.pose();
		Matrix3f matrix3f = lastPose.normal();
		VertexConsumer vertCon = buffSource.getBuffer(RENDER_TYPE);
		vertex(vertCon, matrix4f, matrix3f, packedLighting, 0, 0, 0, 1);
		vertex(vertCon, matrix4f, matrix3f, packedLighting, 1, 0, 1, 1);
		vertex(vertCon, matrix4f, matrix3f, packedLighting, 1, 1, 1, 0);
		vertex(vertCon, matrix4f, matrix3f, packedLighting, 0, 1, 0, 0);
		poseStack.popPose();
		super.render(entity, p_114081_, partialTick, poseStack, buffSource, packedLighting);
		
		if (entity.hasFlame())
			this.renderFlame(poseStack, buffSource, entity, Mth.rotationAroundAxis(Mth.Y_AXIS, this.entityRenderDispatcher.cameraOrientation(), new Quaternionf()));
	}

	private static void vertex(VertexConsumer p_114090_, Matrix4f matrix4, Matrix3f matrix3, int packedLight, int x, int y, int u, int v)
	{
		p_114090_.vertex(matrix4, x - 0.5F, y - 0.25F, 0.0F).color(255, 255, 255, 255).uv(u, v).overlayCoords(OverlayTexture.NO_OVERLAY).uv2(packedLight).normal(matrix3, 0.0F, 1.0F, 0.0F).endVertex();
	}

	@Override
	public ResourceLocation getTextureLocation(SoulFireballEntity entity)
	{
		return TEXTURE_LOCATION;
	}
	
	public static final Material SOUL_FIRE_0 = new Material(InventoryMenu.BLOCK_ATLAS, new ResourceLocation("block/soul_fire_0"));
    public static final Material SOUL_FIRE_1 = new Material(InventoryMenu.BLOCK_ATLAS, new ResourceLocation("block/soul_fire_1"));

	private void renderFlame(PoseStack poseStack, MultiBufferSource buffSource, Entity entity, Quaternionf quaternion)
	{
		TextureAtlasSprite textureatlassprite = SOUL_FIRE_0.sprite();
		TextureAtlasSprite textureatlassprite1 = SOUL_FIRE_1.sprite();
		poseStack.pushPose();
		float scale = entity.getBbWidth() * 2.4F;
		poseStack.scale(scale, scale, scale);
		float width = 0.5F;
		float height = entity.getBbHeight() / scale;
		float depth = 0.0F;
		poseStack.mulPose(quaternion);
		poseStack.translate(0.0F, -0.2F, -0.3F + (float) ((int) height) * 0.02F);
		float f5 = 0.0F;
		int i = 0;
		VertexConsumer vertexconsumer = buffSource.getBuffer(Sheets.cutoutBlockSheet());

		for (PoseStack.Pose posestack$pose = poseStack.last(); height > 0.0F; ++i)
		{
			TextureAtlasSprite textureatlassprite2 = i % 2 == 0 ? textureatlassprite : textureatlassprite1;
			float f6 = textureatlassprite2.getU0();
			float f7 = textureatlassprite2.getV0();
			float f8 = textureatlassprite2.getU1();
			float f9 = textureatlassprite2.getV1();
			if (i / 2 % 2 == 0)
			{
				float f10 = f8;
				f8 = f6;
				f6 = f10;
			}

			fireVertex(posestack$pose, vertexconsumer, width - 0.0F, 0.0F - depth, f5, f8, f9);
			fireVertex(posestack$pose, vertexconsumer, -width - 0.0F, 0.0F - depth, f5, f6, f9);
			fireVertex(posestack$pose, vertexconsumer, -width - 0.0F, 1.4F - depth, f5, f6, f7);
			fireVertex(posestack$pose, vertexconsumer, width - 0.0F, 1.4F - depth, f5, f8, f7);
			height -= 0.45F;
			depth -= 0.45F;
			width *= 0.9F;
			f5 += 0.03F;
		}

		poseStack.popPose();
	}

	private static void fireVertex(PoseStack.Pose pMatrixEntry, VertexConsumer pBuffer, float pX, float pY, float pZ, float pTexU, float pTexV)
	{
		pBuffer.vertex(pMatrixEntry.pose(), pX, pY, pZ).color(255, 255, 255, 255).uv(pTexU, pTexV).overlayCoords(0, 10).uv2(240).normal(pMatrixEntry.normal(), 0.0F, 1.0F, 0.0F).endVertex();
	}
}
