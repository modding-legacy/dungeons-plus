package com.legacy.dungeons_plus.events;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import com.legacy.dungeons_plus.DPConfig;
import com.legacy.dungeons_plus.DungeonsPlus;
import com.legacy.dungeons_plus.data.providers.DPAdvancementProv;
import com.legacy.dungeons_plus.data.providers.DPLangProvider;
import com.legacy.dungeons_plus.data.providers.DPLootProv;
import com.legacy.dungeons_plus.data.providers.DPModelProv;
import com.legacy.dungeons_plus.data.providers.DPRecipeProvider;
import com.legacy.dungeons_plus.data.providers.DPTagProv;
import com.legacy.dungeons_plus.registry.DPBlocks;
import com.legacy.dungeons_plus.registry.DPDamageSources;
import com.legacy.dungeons_plus.registry.DPItems;
import com.legacy.dungeons_plus.registry.DPLoot;
import com.legacy.dungeons_plus.registry.DPStructures;
import com.legacy.structure_gel.api.data.providers.NestedDataProvider;
import com.legacy.structure_gel.api.entity.EntityAccessHelper;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;
import com.legacy.structure_gel.api.registry.registrar.StructureRegistrar;
import com.legacy.structure_gel.api.structure.StructureAccessHelper;

import net.minecraft.DetectedVersion;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.RegistrySetBuilder;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DataProvider;
import net.minecraft.data.PackOutput;
import net.minecraft.data.metadata.PackMetadataGenerator;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.server.packs.PackType;
import net.minecraft.server.packs.metadata.pack.PackMetadataSection;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.InclusiveRange;
import net.minecraft.util.RandomSource;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.CreativeModeTab.TabVisibility;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;
import net.neoforged.bus.api.Event.Result;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.ModList;
import net.neoforged.fml.common.Mod;
import net.neoforged.fml.event.lifecycle.FMLCommonSetupEvent;
import net.neoforged.neoforge.common.data.BlockTagsProvider;
import net.neoforged.neoforge.common.data.DatapackBuiltinEntriesProvider;
import net.neoforged.neoforge.common.data.ExistingFileHelper;
import net.neoforged.neoforge.common.util.MutableHashedLinkedMap;
import net.neoforged.neoforge.data.event.GatherDataEvent;
import net.neoforged.neoforge.event.BuildCreativeModeTabContentsEvent;
import net.neoforged.neoforge.event.entity.EntityJoinLevelEvent;
import net.neoforged.neoforge.event.entity.living.MobEffectEvent;
import net.neoforged.neoforge.event.entity.living.MobSpawnEvent;
import net.neoforged.neoforge.event.level.LevelEvent;

public class DPCommonEvents
{
	@Mod.EventBusSubscriber(modid = DungeonsPlus.MODID, bus = Mod.EventBusSubscriber.Bus.FORGE)
	public static class ForgeBus
	{
		// Fires once per entity when they first spawn. Should only fire for server.
		@SubscribeEvent
		protected static void onFinalizeSpawn(final MobSpawnEvent.FinalizeSpawn event)
		{
			Mob entity = event.getEntity();
			ServerLevelAccessor levelAccessor = event.getLevel();
			ifInStructurePiece(levelAccessor, entity, EntityType.HUSK, DPStructures.LEVIATHAN, e ->
			{
				if (DPConfig.COMMON.husksDropSand.get())
					EntityAccessHelper.setDeathLootTable(e, DPLoot.Leviathan.ENTITY_HUSK);
				RandomSource rand = e.getRandom();
				if (rand.nextFloat() < DPConfig.COMMON.huskLeviathanBladeChance.get())
				{
					var stack = DPItems.LEVIATHAN_BLADE.get().getDefaultInstance();
					stack.setDamageValue(rand.nextInt(stack.getItem().getMaxDamage(stack)));
					e.setItemSlot(EquipmentSlot.MAINHAND, stack);
					e.setDropChance(EquipmentSlot.MAINHAND, 0.12F);
				}
			});
			ifInStructurePiece(levelAccessor, entity, EntityType.STRAY, DPStructures.SNOWY_TEMPLE, e ->
			{
				if (DPConfig.COMMON.straysDropIce.get())
					EntityAccessHelper.setDeathLootTable(e, DPLoot.SnowyTemple.ENTITY_STRAY);
				RandomSource rand = e.getRandom();
				if (rand.nextFloat() < DPConfig.COMMON.strayFrostedCowlChance.get())
				{
					var stack = DPItems.FROSTED_COWL.get().getDefaultInstance();
					stack.setDamageValue(rand.nextInt(stack.getItem().getMaxDamage(stack)));
					e.setItemSlot(EquipmentSlot.HEAD, stack);
					e.setDropChance(EquipmentSlot.HEAD, 0.12F);
				}
			});
			ifInStructurePiece(levelAccessor, entity, EntityType.DROWNED, DPStructures.WARPED_GARDEN, e ->
			{
				RandomSource rand = e.getRandom();
				if (rand.nextFloat() < DPConfig.COMMON.drownedWarpedAxeChance.get())
				{
					var stack = DPItems.WARPED_AXE.get().getDefaultInstance();
					stack.setDamageValue(rand.nextInt(stack.getItem().getMaxDamage(stack)));
					e.setItemSlot(EquipmentSlot.MAINHAND, stack);
					e.setDropChance(EquipmentSlot.MAINHAND, 0.12F);
				}
				if (rand.nextFloat() < DPConfig.COMMON.drownedCoralChance.get())
				{
					var opTag = levelAccessor.registryAccess().registryOrThrow(Registries.BLOCK).getTag(BlockTags.CORAL_BLOCKS);
					if (opTag.isPresent() && opTag.get().size() > 0)
					{
						e.setItemSlot(EquipmentSlot.OFFHAND, new ItemStack(opTag.get().getRandomElement(rand).get().value()));
						e.setDropChance(EquipmentSlot.OFFHAND, 1.0F);
					}
				}
			});
			ifInStructurePiece(levelAccessor, entity, EntityType.SKELETON, DPStructures.SOUL_PRISON, e ->
			{
				RandomSource rand = e.getRandom();
				if (rand.nextFloat() < DPConfig.COMMON.skeletonSoulCannonChance.get())
				{
					var stack = DPItems.SOUL_CANNON.get().getDefaultInstance();
					stack.setDamageValue(rand.nextInt(stack.getItem().getMaxDamage(stack)));
					e.setItemSlot(EquipmentSlot.OFFHAND, stack);
					e.setDropChance(EquipmentSlot.OFFHAND, 0.30F);
				}
			});
		}

		// Fires every time an entity joins the world
		@SubscribeEvent
		protected static void onJoinLevel(final EntityJoinLevelEvent event)
		{
			Entity entity = event.getEntity();
			Level level = event.getLevel();
			if (level instanceof ServerLevelAccessor levelAccessor)
			{
				ifInStructure(levelAccessor, entity, EntityType.GHAST, DPStructures.SOUL_PRISON, e ->
				{
					e.targetSelector.addGoal(1, new NearestAttackableTargetGoal<Player>(e, Player.class, true, false));
				});
				ifInStructurePiece(levelAccessor, entity, EntityType.ENDERMAN, DPStructures.END_RUINS, e ->
				{
					e.targetSelector.addGoal(1, new NearestAttackableTargetGoal<Player>(e, Player.class, true, false));
				});
			}
		}

		@SuppressWarnings("unchecked")
		private static <T extends Entity> void ifInStructurePiece(ServerLevelAccessor levelAccessor, Entity entity, EntityType<T> entityTest, StructureRegistrar<?> structure, Consumer<T> consumer)
		{
			if (entity.getType().equals(entityTest) && StructureAccessHelper.isInStructurePiece(levelAccessor, structure.getType(), entity.blockPosition()))
				consumer.accept((T) entity);
		}

		@SuppressWarnings("unchecked")
		private static <T extends Entity> void ifInStructure(ServerLevelAccessor levelAccessor, Entity entity, EntityType<T> entityTest, StructureRegistrar<?> structure, Consumer<T> consumer)
		{
			if (entity.getType().equals(entityTest) && StructureAccessHelper.isInStructure(levelAccessor, structure.getType(), entity.blockPosition()))
				consumer.accept((T) entity);
		}

		@SubscribeEvent
		protected static void onEffectApply(final MobEffectEvent.Applicable event)
		{
			if (event.getEffectInstance().getEffect() == MobEffects.MOVEMENT_SLOWDOWN)
			{
				List<ItemStack> strayArmors = new ArrayList<>(1);
				LivingEntity entity = event.getEntity();
				for (ItemStack stack : entity.getArmorSlots())
					if (stack.getItem() instanceof ArmorItem armor && armor.getMaterial() == DPItems.DPArmors.STRAY)
						strayArmors.add(stack);

				int size = strayArmors.size();
				if (size > 0)
				{
					event.setResult(Result.DENY);
					if (entity instanceof ServerPlayer serverPlayer)
					{
						RandomSource rand = serverPlayer.getRandom();
						strayArmors.get(rand.nextInt(size)).hurt(2, rand, serverPlayer);
					}
				}
			}
		}

		@SubscribeEvent
		protected static void onLevelLoad(final LevelEvent.Load event)
		{
			if (event.getLevel() instanceof Level level && level.dimension().equals(Level.OVERWORLD))
			{
				DPDamageSources.instance = new DPDamageSources(level.registryAccess());
			}
		}
	}

	@Mod.EventBusSubscriber(modid = DungeonsPlus.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
	public static class ModBus
	{
		@SubscribeEvent
		protected static void commonInit(final FMLCommonSetupEvent event)
		{
			ModList modList = ModList.get();
			DungeonsPlus.isWaystonesLoaded = modList.isLoaded("waystones");
		}

		@SubscribeEvent
		protected static void commonInit(final BuildCreativeModeTabContentsEvent event)
		{
			var tab = event.getTabKey();
			MutableHashedLinkedMap<ItemStack, TabVisibility> entries = event.getEntries();

			if (tab.equals(CreativeModeTabs.COMBAT))
			{
				event.accept(DPItems.FROSTED_COWL);
				event.accept(DPItems.LEVIATHAN_BLADE);
				event.accept(DPItems.WARPED_AXE);
				event.accept(DPItems.SOUL_CANNON);
			}
			else if (tab.equals(CreativeModeTabs.NATURAL_BLOCKS))
			{
				insertAfter(entries, List.of(DPBlocks.GRANITE_IRON_ORE.get()), Items.IRON_ORE);
				insertAfter(entries, List.of(DPBlocks.GRANITE_GOLD_ORE.get()), Items.GOLD_ORE);
			}
		}

		protected static void insertAfter(MutableHashedLinkedMap<ItemStack, TabVisibility> entries, List<ItemLike> items, ItemLike target)
		{
			ItemStack currentStack = null;

			for (var e : entries)
			{
				if (e.getKey().getItem() == target)
				{
					currentStack = e.getKey();
					break;
				}
			}

			for (var item : items)
				entries.putAfter(currentStack, currentStack = item.asItem().getDefaultInstance(), TabVisibility.PARENT_AND_SEARCH_TABS);
		}

		@SubscribeEvent
		protected static void gatherData(final GatherDataEvent event)
		{
			DataGenerator gen = event.getGenerator();
			PackOutput output = gen.getPackOutput();
			ExistingFileHelper existingFileHelper = event.getExistingFileHelper();
			boolean server = event.includeServer();
			boolean client = event.includeClient();

			// data
			DatapackBuiltinEntriesProvider registrarProv = new DatapackBuiltinEntriesProvider(output, event.getLookupProvider(), RegistrarHandler.injectRegistries(new RegistrySetBuilder()), Set.of(DungeonsPlus.MODID));
			gen.addProvider(server, registrarProv);
			CompletableFuture<HolderLookup.Provider> lookup = registrarProv.getRegistryProvider();

			BlockTagsProvider blockTagProv = new DPTagProv.BlockProv(output, lookup, existingFileHelper);
			gen.addProvider(server, blockTagProv);
			gen.addProvider(server, new DPTagProv.ItemProv(output, lookup, blockTagProv.contentsGetter(), existingFileHelper));
			gen.addProvider(server, new DPTagProv.EntityTypeProv(output, lookup, existingFileHelper));
			gen.addProvider(server, new DPTagProv.StructureProv(output, lookup, existingFileHelper));
			gen.addProvider(server, new DPTagProv.BiomeProv(output, lookup, existingFileHelper));
			gen.addProvider(server, new DPTagProv.EnchantmentProv(output, lookup, existingFileHelper));
			gen.addProvider(server, new DPTagProv.DamageTypeProv(output, lookup, existingFileHelper));

			gen.addProvider(server, new DPAdvancementProv(output, lookup, existingFileHelper));
			gen.addProvider(server, new DPLootProv(output));
			gen.addProvider(server, new DPRecipeProvider(output, lookup));

			// assets
			gen.addProvider(client, new DPLangProvider(output, lookup));
			gen.addProvider(client, new DPModelProv.States(output, existingFileHelper));
			gen.addProvider(client, new DPModelProv.ItemModels(output, existingFileHelper));

			// pack
			gen.addProvider(server, packMcmeta(output, "Dungeons Plus resources"));
		}

		private static final DataProvider packMcmeta(PackOutput output, String description)
		{
			int serverVersion = DetectedVersion.BUILT_IN.getPackVersion(PackType.SERVER_DATA);
			return NestedDataProvider.of(new PackMetadataGenerator(output).add(PackMetadataSection.TYPE, new PackMetadataSection(Component.literal(description), serverVersion, Optional.of(new InclusiveRange<>(0, Integer.MAX_VALUE)))), description);
		}
	}
}
