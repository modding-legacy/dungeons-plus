package com.legacy.dungeons_plus;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.legacy.structure_gel.core.util.LoggerWrapper;

import net.minecraft.resources.ResourceLocation;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.fml.ModList;
import net.neoforged.fml.ModLoadingContext;
import net.neoforged.fml.common.Mod;
import net.neoforged.fml.config.ModConfig;
import net.neoforged.neoforge.common.util.Lazy;

@Mod(DungeonsPlus.MODID)
public class DungeonsPlus
{
	public static final String MODID = "dungeons_plus";
	public static final LoggerWrapper LOGGER = new LoggerWrapper(MODID);
	public static final Lazy<IEventBus> EVENT_BUS = () -> ModList.get().getModContainerById(MODID).get().getEventBus();
	
	public static boolean isWaystonesLoaded = false;

	public DungeonsPlus(IEventBus modBus)
	{		
		ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, DPConfig.COMMON_SPEC);
	}

	public static ResourceLocation locate(String key)
	{
		return new ResourceLocation(MODID, key);
	}

	public static ResourceLocation[] locateAll(String... keys)
	{
		return Arrays.stream(keys).map(DungeonsPlus::locate).toArray(ResourceLocation[]::new);
	}

	public static ResourceLocation[] locateAllPrefix(String prefix, String... keys)
	{
		return Arrays.stream(keys).map(s -> DungeonsPlus.locate(prefix + s)).toArray(ResourceLocation[]::new);
	}

	public static Logger makeLogger(Class<?> containerClass)
	{
		return LogManager.getLogger("ModdingLegacy/" + MODID + "/" + containerClass.getSimpleName());
	}
}
