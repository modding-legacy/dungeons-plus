package com.legacy.dungeons_plus.data.advancement;

import java.util.Optional;

import com.legacy.dungeons_plus.registry.DPCriteriaTriggers;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.advancements.Criterion;
import net.minecraft.advancements.critereon.ContextAwarePredicate;
import net.minecraft.advancements.critereon.ItemPredicate;
import net.minecraft.advancements.critereon.SimpleCriterionTrigger;
import net.minecraft.advancements.critereon.StatePropertiesPredicate;
import net.minecraft.core.Holder;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.util.ExtraCodecs;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;

public class ThrownItemHitBlockTrigger extends SimpleCriterionTrigger<ThrownItemHitBlockTrigger.TriggerInstance>
{
	@Override
	public Codec<TriggerInstance> codec()
	{
		return TriggerInstance.CODEC;
	}

	public void trigger(ServerPlayer player, ItemStack stack, BlockState state)
	{
		this.trigger(player, trigger ->
		{
			return trigger.matches(stack, state);
		});
	}

	public static record TriggerInstance(Optional<ItemPredicate> item, Optional<Holder<Block>> block, Optional<StatePropertiesPredicate> state) implements SimpleCriterionTrigger.SimpleInstance
	{

		public static final Codec<TriggerInstance> CODEC = RecordCodecBuilder.create(instance ->
		{
			//@formatter:off
			return instance.group(
					ExtraCodecs.strictOptionalField(ItemPredicate.CODEC, "item").forGetter(TriggerInstance::item),
					ExtraCodecs.strictOptionalField(BuiltInRegistries.BLOCK.holderByNameCodec(), "block").forGetter(TriggerInstance::block),
					ExtraCodecs.strictOptionalField(StatePropertiesPredicate.CODEC, "state").forGetter(TriggerInstance::state)).apply(instance, TriggerInstance::new);
			//@formatter:on
		});

		public static Criterion<ThrownItemHitBlockTrigger.TriggerInstance> of(Optional<ItemPredicate> itemPredicate, Optional<Holder<Block>> block)
		{
			return DPCriteriaTriggers.THROWN_ITEM_HIT_BLOCK.get().createCriterion(new ThrownItemHitBlockTrigger.TriggerInstance(itemPredicate, block, Optional.empty()));
		}

		public static Criterion<ThrownItemHitBlockTrigger.TriggerInstance> hitsBlock(Optional<Holder<Block>> block)
		{
			return of(Optional.empty(), block);
		}

		public static Criterion<ThrownItemHitBlockTrigger.TriggerInstance> withItem(Optional<ItemPredicate> itemPredicate)
		{
			return of(itemPredicate, null);
		}

		public boolean matches(ItemStack stack, BlockState state)
		{
			boolean itemMatches = this.item.isEmpty() ? true : this.item.get().matches(stack);
			boolean blockMatches = this.block.isEmpty() ? true : state.is(this.block.get());
			boolean stateMatches = this.state.isEmpty() ? true : this.state.get().matches(state);
			return itemMatches && blockMatches && stateMatches;
		}

		@Override
		public Optional<ContextAwarePredicate> player()
		{
			return Optional.empty();
		}
	}
}