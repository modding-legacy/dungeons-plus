package com.legacy.dungeons_plus.data.providers;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

import javax.annotation.Nullable;

import com.legacy.dungeons_plus.DungeonsPlus;
import com.legacy.dungeons_plus.registry.DPBlocks;
import com.legacy.dungeons_plus.registry.DPItems;

import net.minecraft.core.Direction;
import net.minecraft.core.Direction.Axis;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemDisplayContext;
import net.minecraft.world.item.SpawnEggItem;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.ButtonBlock;
import net.minecraft.world.level.block.DoorBlock;
import net.minecraft.world.level.block.DoublePlantBlock;
import net.minecraft.world.level.block.FenceBlock;
import net.minecraft.world.level.block.FenceGateBlock;
import net.minecraft.world.level.block.LadderBlock;
import net.minecraft.world.level.block.LeavesBlock;
import net.minecraft.world.level.block.PressurePlateBlock;
import net.minecraft.world.level.block.RotatedPillarBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.StairBlock;
import net.minecraft.world.level.block.StandingSignBlock;
import net.minecraft.world.level.block.TrapDoorBlock;
import net.minecraft.world.level.block.VineBlock;
import net.minecraft.world.level.block.WallBlock;
import net.minecraft.world.level.block.WallSignBlock;
import net.minecraft.world.level.block.state.properties.DoubleBlockHalf;
import net.neoforged.neoforge.client.model.generators.BlockModelBuilder;
import net.neoforged.neoforge.client.model.generators.BlockStateProvider;
import net.neoforged.neoforge.client.model.generators.ConfiguredModel;
import net.neoforged.neoforge.client.model.generators.ItemModelBuilder;
import net.neoforged.neoforge.client.model.generators.ItemModelProvider;
import net.neoforged.neoforge.client.model.generators.ModelFile;
import net.neoforged.neoforge.client.model.generators.ModelProvider;
import net.neoforged.neoforge.client.model.generators.loaders.SeparateTransformsModelBuilder;
import net.neoforged.neoforge.common.data.ExistingFileHelper;

public class DPModelProv
{
	private static final String GENERATED = "item/generated";
	private static final String HANDHELD = "item/handheld";

	protected static List<Block> getAllBlocks()
	{
		return BuiltInRegistries.BLOCK.stream().filter(block -> BuiltInRegistries.BLOCK.getKey(block).getNamespace().equals(DungeonsPlus.MODID)).toList();
	}

	protected static List<Item> getAllItems(Function<Item, Boolean> condition)
	{
		return BuiltInRegistries.ITEM.stream().filter(item -> BuiltInRegistries.ITEM.getKey(item).getNamespace().equals(DungeonsPlus.MODID) && condition.apply(item)).toList();
	}

	protected static List<Item> getAllItems()
	{
		return getAllItems(i -> true);
	}

	public static class ItemModels extends ItemModelProvider
	{
		public ItemModels(PackOutput output, ExistingFileHelper existingFileHelper)
		{
			super(output, DungeonsPlus.MODID, existingFileHelper);
		}

		@Override
		protected void registerModels()
		{
			this.handheldItem(DPItems.WARPED_AXE.get());

			this.tintedOverlay(DPItems.FROSTED_COWL.get());

			this.scaledHandheld(DPItems.LEVIATHAN_BLADE.get(), 1.105F, 0.68F, 6.0F);

			this.soulCannon(DPItems.SOUL_CANNON.get());

			// basic spawn eggs
			getAllItems(i -> i instanceof SpawnEggItem).forEach(item -> this.withExistingParent(BuiltInRegistries.ITEM.getKey(item).getPath(), new ResourceLocation("item/template_spawn_egg")));

		}

		public ItemModelBuilder scaledHandheld(Item item, float thirdPersonScale, float firstPersonScale, float thirdPersonTranslation)
		{
			var model = this.handheldItem(item);
			var transforms = model.transforms();

			transforms.transform(ItemDisplayContext.THIRD_PERSON_RIGHT_HAND).rotation(0.0F, -90.0F, 55.0F).translation(0.0F, thirdPersonTranslation, 0.5F).scale(thirdPersonScale);
			transforms.transform(ItemDisplayContext.THIRD_PERSON_LEFT_HAND).rotation(0.0F, 90.0F, -55.0F).translation(0.0F, thirdPersonTranslation, 0.5F).scale(thirdPersonScale);
			transforms.transform(ItemDisplayContext.FIRST_PERSON_RIGHT_HAND).rotation(0.0F, -90.0F, 25.0F).translation(1.13F, 3.2F, 1.13F).scale(firstPersonScale);
			transforms.transform(ItemDisplayContext.FIRST_PERSON_LEFT_HAND).rotation(0.0F, 90.0F, -25.0F).translation(1.13F, 3.2F, 1.13F).scale(firstPersonScale);

			return transforms.end();
		}

		public ItemModelBuilder soulCannon(Item item)
		{
			String tex = "tex";
			String texKey = "#" + tex;
			String particle = "particle";
			String name = this.name(item);
			ResourceLocation invTexture = texLoc(name).withSuffix("_inventory");

			//@formatter:off
			ItemModelBuilder model = this.withExistingParent(name, new ResourceLocation("neoforge", "item/default"))
				.texture(particle, invTexture);
			
			ItemModelBuilder inventory = this.getBuilder(name + "_inventory").parent(new ModelFile.UncheckedModelFile(GENERATED))
				.texture("layer0", invTexture);
			
			ItemModelBuilder thirdPerson = this.getBuilder(name + "_held")
				.texture(tex, this.texLoc(name))
				.texture(particle, invTexture)
				.element()
					.from(5.5F, 0.0F, 5.5f)
					.to(10.5f, 9f, 10.5f)
					.face(Direction.NORTH).uvs(0, 5, 5, 14).texture(texKey).end()
					.face(Direction.EAST).uvs(10, 5, 5, 14).texture(texKey).end()
					.face(Direction.SOUTH).uvs(10, 5, 15, 14).texture(texKey).end()
					.face(Direction.WEST).uvs(5, 5, 10, 14).texture(texKey).end()
					.face(Direction.UP).uvs(10, 0, 15, 5).texture(texKey).end()
					.face(Direction.DOWN).uvs(0, 0, 5, 5).texture(texKey).end()
					.end()
				.transforms()
					.transform(ItemDisplayContext.THIRD_PERSON_RIGHT_HAND)
						.rotation(-90F, -180F, 0F)
						.translation(0F, -2F, -1.75F)
						.end()
					.transform(ItemDisplayContext.THIRD_PERSON_LEFT_HAND)
						.rotation(-90F, -180F, 0F)
						.translation(0F, -2F, -1.75F)
						.end()
					.transform(ItemDisplayContext.FIRST_PERSON_RIGHT_HAND)
						.rotation(-69F, -171F, 14F)
						.translation(0.75F, 2.5F, -2F)
						.end()
					.transform(ItemDisplayContext.FIRST_PERSON_LEFT_HAND)
						.rotation(-69F, -171F, 14F)
						.translation(0.75F, 2.5F, -2F)
						.end()
					.transform(ItemDisplayContext.GROUND)
						.rotation(0F, 0F, -90F)
						.translation(2.5F, 0F, 0F)
						.scale(0.75F, 0.75F, 0.75F)
						.end()
					.transform(ItemDisplayContext.GUI)
						.rotation(-61, -180, 152)
						.translation(2, -1.75F, 0)
						.scale(1.3F, 1.3F, 1.3F)
						.end()
					.transform(ItemDisplayContext.HEAD)
						.rotation(90, 0, -180)
						.translation(0, -3.75F, -8.75F)
						.end()
					.transform(ItemDisplayContext.FIXED)
						.rotation(90, 0, -180)
						.translation(0, 0, -6)
						.end()
					.end();
			
			String fp = "_first_person";
			ItemModelBuilder firstPerson = this.withExistingParent(name + fp, thirdPerson.getLocation())
				.texture(tex, this.texLoc(name + fp))
				.texture(particle, invTexture);
			
			SeparateTransformsModelBuilder<ItemModelBuilder> loader = model.customLoader(SeparateTransformsModelBuilder::begin);
			// These new ItemModelBuilder instances don't get generated
			loader.base(new ItemModelBuilder(inventory.getLocation(), existingFileHelper).parent(inventory));
			loader.perspective(ItemDisplayContext.THIRD_PERSON_LEFT_HAND, new ItemModelBuilder(thirdPerson.getLocation(), existingFileHelper).parent(thirdPerson));
			loader.perspective(ItemDisplayContext.THIRD_PERSON_RIGHT_HAND, new ItemModelBuilder(thirdPerson.getLocation(), existingFileHelper).parent(thirdPerson));
			loader.perspective(ItemDisplayContext.FIRST_PERSON_LEFT_HAND, new ItemModelBuilder(firstPerson.getLocation(), existingFileHelper).parent(firstPerson));
			loader.perspective(ItemDisplayContext.FIRST_PERSON_RIGHT_HAND, new ItemModelBuilder(firstPerson.getLocation(), existingFileHelper).parent(firstPerson));
			loader.perspective(ItemDisplayContext.HEAD, new ItemModelBuilder(thirdPerson.getLocation(), existingFileHelper).parent(thirdPerson));
			//@formatter:on
			return model;
		}

		public ItemModelBuilder tintedOverlay(Item item)
		{
			return tintedOverlay(BuiltInRegistries.ITEM.getKey(item));
		}

		public ItemModelBuilder tintedOverlay(ResourceLocation id)
		{
			ResourceLocation texture = new ResourceLocation(id.getNamespace(), "item/" + id.getPath());
			return getBuilder(id.toString()).parent(new ModelFile.UncheckedModelFile(GENERATED)).texture("layer0", texture).texture("layer1", texture.withSuffix("_overlay"));
		}

		public ItemModelBuilder handheldItem(Item item)
		{
			return handheldItem(Objects.requireNonNull(BuiltInRegistries.ITEM.getKey(item)));
		}

		public ItemModelBuilder handheldItem(ResourceLocation item)
		{
			return getBuilder(item.toString()).parent(new ModelFile.UncheckedModelFile(HANDHELD)).texture("layer0", item.withPrefix("item/"));
		}

		public ItemModelBuilder basicItemInLoc(String name, ResourceLocation item)
		{
			return getBuilder(name.replace("block/", "item/")).parent(new ModelFile.UncheckedModelFile(GENERATED)).texture("layer0", item);
		}

		public ItemModelBuilder basicItemInLoc(ResourceLocation item)
		{
			return this.basicItemInLoc(item.toString(), item);
		}

		public ResourceLocation texLoc(String key)
		{
			return this.modLoc(ModelProvider.ITEM_FOLDER + "/" + key);
		}

		public ResourceLocation mcTexLoc(String key)
		{
			return this.mcLoc(ModelProvider.ITEM_FOLDER + "/" + key);
		}

		private ResourceLocation key(Item item)
		{
			return BuiltInRegistries.ITEM.getKey(item);
		}

		private String name(Item item)
		{
			return key(item).getPath();
		}

	}

	/*public static class BlockModels extends BlockModelProvider
	{
		public BlockModels(PackOutput output, ExistingFileHelper existingFileHelper)
		{
			super(output, Rediscovered.MODID, existingFileHelper);
		}
	
		@Override
		protected void registerModels()
		{
		}
	}*/

	public static class States extends BlockStateProvider
	{
		public States(PackOutput output, ExistingFileHelper existingFileHelper)
		{
			super(output, DungeonsPlus.MODID, existingFileHelper);
		}

		@Override
		protected void registerStatesAndModels()
		{
			this.simpleBlock(DPBlocks.GRANITE_IRON_ORE.get());
			this.simpleBlock(DPBlocks.GRANITE_GOLD_ORE.get());

			// generate default items
			for (Block block : this.registeredBlocks.keySet())
			{
				boolean contained = !this.itemModels().generatedModels.containsKey(this.modLoc(ModelProvider.BLOCK_FOLDER + "/" + this.name(block))) && !this.itemModels().generatedModels.containsKey(this.modLoc(ModelProvider.ITEM_FOLDER + "/" + this.name(block)));
				var possibleItem = BuiltInRegistries.ITEM.get(this.key(block));

				if (possibleItem != null && possibleItem != Blocks.AIR.asItem() && contained)
				{
					try
					{
						/*System.out.println("Generating " + this.name(block));*/
						this.itemModels().withExistingParent(this.name(block), this.modLoc(ModelProvider.BLOCK_FOLDER + "/" + this.name(block)));
					}
					catch (Exception e)
					{
						/*System.out.println("Skipped " + this.name(block));*/
					}
				}
			}
		}

		/**
		 * @param block
		 * @param topTextureParent
		 *            Used for "wood" blocks, this will set the top texture to that of
		 *            another block
		 */
		public void pillarBlock(Block block, Block topTextureParent)
		{
			this.pillarBlock(block, topTextureParent, null);
		}

		public void pillarBlock(Block block, Block textureParent, @Nullable String renderType)
		{
			ResourceLocation baseName = this.blockTexture(textureParent != null ? textureParent : block);

			if (block instanceof RotatedPillarBlock pillar)
			{
				var side = extend(baseName, "_side");
				axisBlockWithRenderType(pillar, side, textureParent != null ? side : extend(baseName, "_top"), renderType != null ? renderType : "solid");
			}
			else
				DungeonsPlus.LOGGER.error("Tried to datagen model for non pillar block: {}", baseName);
		}

		public void door(Block block, Item item, @Nullable String renderType)
		{
			if (block instanceof DoorBlock door)
			{
				this.doorBlockWithRenderType(door, this.extend(this.blockTexture(door), "_lower"), this.extend(this.blockTexture(door), "_upper"), renderType != null ? renderType : "cutout");
				this.itemModels().basicItem(item);
			}
		}

		public void trapDoor(Block block, boolean orientable)
		{
			if (block instanceof TrapDoorBlock door)
			{
				this.trapdoorBlockWithRenderType(door, this.blockTexture(door), orientable, "cutout");
				this.itemModels().withExistingParent(this.name(door), this.extend(this.blockTexture(door), "_bottom"));
			}
		}

		public void trapDoor(Block block, boolean orientable, @Nullable String renderType)
		{
			if (block instanceof TrapDoorBlock door)
			{
				this.trapdoorBlockWithRenderType(door, this.blockTexture(door), orientable, renderType != null ? renderType : "cutout");
				this.itemModels().withExistingParent(this.name(door), this.extend(this.blockTexture(door), "_bottom"));
			}
		}

		public void vine(Block b)
		{
			var vineModel = this.models().withExistingParent(name(b), "block/vine").texture("vine", blockTexture(b)).texture("particle", "#vine").renderType("cutout");
			var vineStateBuilder = this.getMultipartBuilder(b);

			for (Direction dir : VineBlock.PROPERTY_BY_DIRECTION.keySet().stream().sorted().toList())
			{
				int y = (dir.getOpposite().get2DDataValue() * 90);
				vineStateBuilder.part().modelFile(vineModel).uvLock(true).rotationX(dir == Direction.UP ? 270 : 0).rotationY(y % 360).addModel().condition(VineBlock.PROPERTY_BY_DIRECTION.get(dir), true).end();
			}

			this.item(b.asItem(), true);
		}

		public void cross(Block block)
		{
			this.cross(block, this.name(block), true);
		}

		public void cross(Block block, String name, boolean item)
		{
			this.simpleBlock(block, this.models().withExistingParent(name, this.mcBlockFolder("cross")).texture("cross", this.blockFolder(name)).renderType("cutout"));

			if (item)
				this.item(block.asItem(), true);
		}

		public void pottedCross(Block block, ResourceLocation texture)
		{
			if (texture == null)
			{
				texture = this.blockTexture(block);
				texture = texture.withPath(texture.getPath().replace("potted_", ""));
			}

			var model = this.models().withExistingParent(this.name(block), this.mcBlockFolder("flower_pot_cross")).texture("plant", texture).renderType("cutout");

			this.simpleBlock(block, model);
		}

		public void tallCross(Block block)
		{
			BlockModelBuilder top = this.models().withExistingParent(this.name(block).concat("_top"), this.mcBlockFolder("cross")).renderType("cutout");
			BlockModelBuilder bottom = this.models().withExistingParent(this.name(block).concat("_bottom"), this.mcBlockFolder("cross")).renderType("cutout");

			var topTex = this.extend(this.blockTexture(block), "_top");
			var bottomTex = this.extend(this.blockTexture(block), "_bottom");

			top = top.texture("cross", topTex);
			bottom = bottom.texture("cross", bottomTex);

			BlockModelBuilder topFinal = top;
			BlockModelBuilder bottomFinal = bottom;

			getVariantBuilder(block).forAllStatesExcept(state -> ConfiguredModel.builder().modelFile(state.getValue(DoublePlantBlock.HALF) == DoubleBlockHalf.UPPER ? topFinal : bottomFinal).build());

			this.item(block.asItem(), this.extend(BuiltInRegistries.ITEM.getKey(block.asItem()), "_top"), true);
		}

		public void stairs(Block block, Block parent)
		{
			if (block instanceof StairBlock stairs)
			{
				var tex = blockTexture(parent);
				this.stairsBlock(stairs, tex);
			}
			else
				DungeonsPlus.LOGGER.error("Tried to datagen model for non stairs block: {}", this.key(block));
		}

		public void slab(Block block, Block parent)
		{
			if (block instanceof SlabBlock slab)
			{
				super.slabBlock(slab, this.blockTexture(parent), this.blockTexture(parent));
			}
			else
				DungeonsPlus.LOGGER.error("Tried to datagen model for non slab block: {}", this.key(block));
		}

		public void fences(Block block, Block gate, Block parent)
		{
			var tex = this.blockTexture(parent);

			if (block instanceof FenceBlock fence)
			{
				this.fenceBlock(fence, tex);
				this.itemModels().fenceInventory(this.name(fence), tex);
			}
			else
				DungeonsPlus.LOGGER.error("Tried to datagen model for non fence block: {}", this.key(block));

			if (gate instanceof FenceGateBlock fence)
				this.fenceGateBlock(fence, tex);
			else
				DungeonsPlus.LOGGER.error("Tried to datagen model for non gate block: {}", this.key(gate));
		}

		public void wall(Block block, Block parent)
		{
			this.wall(block, this.blockTexture(parent), null);
		}

		public void wall(Block block, ResourceLocation tex, @Nullable String renderType)
		{
			if (block instanceof WallBlock wall)
			{
				if (renderType != null)
					this.wallBlockWithRenderType(wall, tex, renderType);
				else
					this.wallBlock(wall, tex);

				this.itemModels().wallInventory(this.name(wall), tex);
			}
			else
				DungeonsPlus.LOGGER.error("Tried to datagen model for non wall block: {}", this.key(block));
		}

		public void button(ButtonBlock block, ResourceLocation texture, @Nullable String renderType)
		{
			this.buttonBlock(block, texture);
			this.itemModels().buttonInventory(this.name(block), texture);
		}

		public void sign(StandingSignBlock signBlock, WallSignBlock wallSignBlock, ResourceLocation texture)
		{
			this.signBlock(signBlock, wallSignBlock, texture);
			this.item(signBlock.asItem(), false);
		}

		public void craftingTable(Block block, Block bottom)
		{
			var texture = this.blockTexture(block);
			this.simpleBlock(block, this.models().orientableWithBottom(this.name(block), this.extend(texture, "_side"), this.extend(texture, "_front"), this.blockTexture(bottom), this.extend(texture, "_top")));
		}

		public void ladder(Block block)
		{
			var texture = this.blockTexture(block);

			var vineModel = this.models().withExistingParent(name(block), "block/ladder").texture("texture", texture).texture("particle", "#texture").renderType("cutout");
			var ladderStateBuilder = this.getMultipartBuilder(block);

			for (Direction dir : Arrays.stream(Direction.values()).filter(d -> d.getAxis() != Axis.Y).toArray(Direction[]::new))
			{
				int y = (dir.getOpposite().get2DDataValue() * 90);
				ladderStateBuilder.part().modelFile(vineModel).uvLock(true).rotationX(0).rotationY(y % 360).addModel().condition(LadderBlock.FACING, dir).end();
			}

			this.item(block.asItem(), true);
		}

		public void pressurePlate(PressurePlateBlock block, ResourceLocation texture, @Nullable String renderType)
		{
			BlockModelBuilder pressurePlate = models().pressurePlate(name(block), texture);
			BlockModelBuilder pressurePlateDown = models().pressurePlateDown(name(block) + "_down", texture);

			if (renderType != null)
			{
				pressurePlate.renderType(renderType);
				pressurePlateDown.renderType(renderType);
			}

			pressurePlate(block, pressurePlate, pressurePlateDown);
		}

		public void pressurePlate(PressurePlateBlock block, ModelFile pressurePlate, ModelFile pressurePlateDown)
		{
			getVariantBuilder(block).partialState().with(PressurePlateBlock.POWERED, true).addModels(new ConfiguredModel(pressurePlateDown)).partialState().with(PressurePlateBlock.POWERED, false).addModels(new ConfiguredModel(pressurePlate));
		}

		public void blockEntity(Block block, Block parent)
		{
			this.simpleBlock(block, this.models().withExistingParent(this.name(block), this.key(parent)));
			this.itemModels().withExistingParent(this.name(block), this.key(Blocks.CHEST));
		}

		public void leaves(LeavesBlock block, String renderType)
		{
			var texture = this.blockTexture(block);
			var model = this.models().withExistingParent(this.name(block), this.mcBlockFolder("leaves")).texture("all", texture);

			if (renderType != null)
				model.renderType(renderType);

			this.simpleBlock(block, model);
		}

		public void carpet(Block block, Block parent)
		{
			var texture = this.blockTexture(parent);
			var model = this.models().withExistingParent(this.name(block), this.mcBlockFolder("carpet")).texture("wool", texture).texture("particle", texture);

			this.simpleBlock(block, model);
		}

		public BlockModelBuilder cube(Block block)
		{
			return this.cube(block, blockTexture(block));
		}

		public BlockModelBuilder cube(Block block, ResourceLocation tex)
		{
			return models().cubeAll(name(block), tex);
		}

		public void bookshelf(Block block, Block plank)
		{
			var plankTex = blockTexture(plank);
			var model = this.models().cubeBottomTop(this.name(block), blockTexture(block), plankTex, plankTex);
			this.simpleBlock(block, model);
		}

		@Override
		public void simpleBlock(Block block)
		{
			this.simpleBlock(block, (String) null);
		}

		public void simpleBlock(Block block, @Nullable String renderType)
		{
			var model = this.cubeAll(block);

			if (renderType != null && model instanceof BlockModelBuilder b)
				b.renderType(renderType);

			super.simpleBlock(block, model);
		}

		private ResourceLocation key(Block block)
		{
			return BuiltInRegistries.BLOCK.getKey(block);
		}

		private String name(Block block)
		{
			return key(block).getPath();
		}

		private ResourceLocation extend(ResourceLocation rl, String suffix)
		{
			return new ResourceLocation(rl.getNamespace(), rl.getPath() + suffix);
		}

		public ResourceLocation blockFolder(String key)
		{
			return this.modLoc(ModelProvider.BLOCK_FOLDER + "/" + key);
		}

		public ResourceLocation mcBlockFolder(String key)
		{
			return this.mcLoc(ModelProvider.BLOCK_FOLDER + "/" + key);
		}

		public ItemModelBuilder item(ItemLike item, boolean blockFolder)
		{
			if (item == null || item == Blocks.AIR.asItem() || item.asItem() == null)
				return null;

			return item(item, BuiltInRegistries.ITEM.getKey(item.asItem()), blockFolder);
		}

		public ItemModelBuilder item(ItemLike item, ResourceLocation texture, boolean blockFolder)
		{
			if (item == null || item == Blocks.AIR.asItem())
				return null;

			String dir = blockFolder ? ModelProvider.BLOCK_FOLDER : ModelProvider.ITEM_FOLDER;
			var key = BuiltInRegistries.ITEM.getKey(item.asItem());
			return this.itemModels().getBuilder(key.toString()).parent(new ModelFile.UncheckedModelFile(GENERATED)).texture("layer0", new ResourceLocation(texture.getNamespace(), dir + "/" + texture.getPath()));
		}

	}
}
