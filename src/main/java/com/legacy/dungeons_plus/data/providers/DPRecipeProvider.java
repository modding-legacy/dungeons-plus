package com.legacy.dungeons_plus.data.providers;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import javax.annotation.Nullable;

import com.google.common.collect.ImmutableList;
import com.legacy.dungeons_plus.DungeonsPlus;
import com.legacy.dungeons_plus.registry.DPBlocks;

import net.minecraft.advancements.AdvancementHolder;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.PackOutput;
import net.minecraft.data.recipes.RecipeBuilder;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.data.recipes.RecipeOutput;
import net.minecraft.data.recipes.ShapedRecipeBuilder;
import net.minecraft.data.recipes.ShapelessRecipeBuilder;
import net.minecraft.data.recipes.SimpleCookingRecipeBuilder;
import net.minecraft.data.recipes.SingleItemRecipeBuilder;
import net.minecraft.data.recipes.packs.VanillaRecipeProvider;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.AbstractCookingRecipe;
import net.minecraft.world.item.crafting.BlastingRecipe;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.SmeltingRecipe;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.SlabBlock;
import net.neoforged.neoforge.common.conditions.ICondition;

public class DPRecipeProvider extends VanillaRecipeProvider
{
	public static final ImmutableList<ItemLike> IRON_SMELTABLES = ImmutableList.of(DPBlocks.GRANITE_IRON_ORE.get());
	public static final ImmutableList<ItemLike> GOLD_SMELTABLES = ImmutableList.of(DPBlocks.GRANITE_GOLD_ORE.get());

	private static final String HAS_ITEM = "has_item";

	public DPRecipeProvider(PackOutput output, CompletableFuture<HolderLookup.Provider> lookupProvider)
	{
		super(output, lookupProvider);
	}

	@Override
	protected CompletableFuture<?> buildAdvancement(CachedOutput output, AdvancementHolder advancementHolder, ICondition... conditions)
	{
		// Don't generate root recipe advancement
		if (RecipeBuilder.ROOT_RECIPE_ADVANCEMENT.equals(advancementHolder.id()))
			return CompletableFuture.completedFuture(null);
		return super.buildAdvancement(output, advancementHolder, conditions);
	}

	@Override
	protected void buildRecipes(RecipeOutput cons)
	{
		oreSmelting(cons, IRON_SMELTABLES, RecipeCategory.MISC, Items.IRON_INGOT, 0.7F, 200, "iron_ingot");
		oreSmelting(cons, GOLD_SMELTABLES, RecipeCategory.MISC, Items.GOLD_INGOT, 1.0F, 200, "gold_ingot");

		oreBlasting(cons, IRON_SMELTABLES, RecipeCategory.MISC, Items.IRON_INGOT, 0.7F, 100, "iron_ingot");
		oreBlasting(cons, GOLD_SMELTABLES, RecipeCategory.MISC, Items.GOLD_INGOT, 1.0F, 100, "gold_ingot");
	}

	protected static void stoneCutting(RecipeOutput cons, RecipeCategory category, ItemLike ingredient, List<ItemLike> results)
	{
		results.forEach(result ->
		{
			SingleItemRecipeBuilder.stonecutting(Ingredient.of(ingredient), category, result, result instanceof SlabBlock ? 2 : 1).unlockedBy(HAS_ITEM, has(ingredient)).save(cons, find(BuiltInRegistries.ITEM.getKey(result.asItem()).getPath() + "_stonecutting_" + BuiltInRegistries.ITEM.getKey(ingredient.asItem()).getPath()));
		});
	}

	protected static void variants(RecipeOutput cons, ItemLike ingredient, @Nullable ItemLike slab, @Nullable ItemLike stairs, @Nullable ItemLike wall)
	{
		if (stairs != null)
			stairBuilder(stairs, Ingredient.of(ingredient)).unlockedBy(HAS_ITEM, has(ingredient)).save(cons);
		if (slab != null)
			slabBuilder(RecipeCategory.BUILDING_BLOCKS, slab, Ingredient.of(ingredient)).unlockedBy(HAS_ITEM, has(ingredient)).save(cons);
		if (wall != null)
			wallBuilder(RecipeCategory.BUILDING_BLOCKS, wall, Ingredient.of(ingredient)).unlockedBy(HAS_ITEM, has(ingredient)).save(cons);
		;
	}

	/*VANILLA COPY START (only change being adding find() to make the files stay in the rediscovered namespace*/
	protected static void oneToOneConversionRecipe(RecipeOutput pFinishedRecipeConsumer, ItemLike pResult, ItemLike pIngredient, @Nullable String pGroup, int pResultCount)
	{
		ShapelessRecipeBuilder.shapeless(RecipeCategory.MISC, pResult, pResultCount).requires(pIngredient).group(pGroup).unlockedBy(getHasName(pIngredient), has(pIngredient)).save(pFinishedRecipeConsumer, find(getConversionRecipeName(pResult, pIngredient)));
	}

	protected <T extends AbstractCookingRecipe> void simpleCookingRecipe(RecipeOutput pFinishedRecipeConsumer, String pCookingMethod, RecipeSerializer<T> pCookingSerializer, int pCookingTime, ItemLike pIngredient, ItemLike pResult, float pExperience, AbstractCookingRecipe.Factory<T> factory)
	{
		SimpleCookingRecipeBuilder.generic(Ingredient.of(pIngredient), RecipeCategory.FOOD, pResult, pExperience, pCookingTime, pCookingSerializer, factory).unlockedBy(getHasName(pIngredient), has(pIngredient)).save(pFinishedRecipeConsumer, find(getItemName(pResult) + "_from_" + pCookingMethod));
	}

	protected static void oreSmelting(RecipeOutput pFinishedRecipeConsumer, List<ItemLike> pIngredients, RecipeCategory pCategory, ItemLike pResult, float pExperience, int pCookingTIme, String pGroup)
	{
		oreCooking(pFinishedRecipeConsumer, RecipeSerializer.SMELTING_RECIPE, pIngredients, pCategory, pResult, pExperience, pCookingTIme, SmeltingRecipe::new, pGroup, "_from_smelting");
	}

	protected static void oreBlasting(RecipeOutput pFinishedRecipeConsumer, List<ItemLike> pIngredients, RecipeCategory pCategory, ItemLike pResult, float pExperience, int pCookingTime, String pGroup)
	{
		oreCooking(pFinishedRecipeConsumer, RecipeSerializer.BLASTING_RECIPE, pIngredients, pCategory, pResult, pExperience, pCookingTime, BlastingRecipe::new, pGroup, "_from_blasting");
	}

	protected static <T extends AbstractCookingRecipe> void oreCooking(RecipeOutput pFinishedRecipeConsumer, RecipeSerializer<T> pCookingSerializer, List<ItemLike> pIngredients, RecipeCategory pCategory, ItemLike pResult, float pExperience, int pCookingTime, AbstractCookingRecipe.Factory<T> factory, String pGroup, String pRecipeName)
	{
		for (ItemLike itemlike : pIngredients)
			SimpleCookingRecipeBuilder.generic(Ingredient.of(itemlike), pCategory, pResult, pExperience, pCookingTime, pCookingSerializer, factory).group(pGroup).unlockedBy(getHasName(itemlike), has(itemlike)).save(pFinishedRecipeConsumer, find(getItemName(pResult) + pRecipeName + "_" + getItemName(itemlike)));
	}

	protected static void nineBlockStorageRecipesRecipesWithCustomUnpacking(RecipeOutput pFinishedRecipeConsumer, RecipeCategory pUnpackedCategory, ItemLike pUnpacked, RecipeCategory pPackedCategory, ItemLike pPacked, String pUnpackedName, String pUnpackedGroup)
	{
		nineBlockStorageRecipes(pFinishedRecipeConsumer, pUnpackedCategory, pUnpacked, pPackedCategory, pPacked, getSimpleRecipeName(pPacked), (String) null, pUnpackedName, pUnpackedGroup);
	}

	protected static void nineBlockStorageRecipes(RecipeOutput pFinishedRecipeConsumer, RecipeCategory pUnpackedCategory, ItemLike pUnpacked, RecipeCategory pPackedCategory, ItemLike pPacked, String pPackedName, @Nullable String pPackedGroup, String pUnpackedName, @Nullable String pUnpackedGroup)
	{
		ShapelessRecipeBuilder.shapeless(pUnpackedCategory, pUnpacked, 9).requires(pPacked).group(pUnpackedGroup).unlockedBy(getHasName(pPacked), has(pPacked)).save(pFinishedRecipeConsumer, pUnpackedName);
		ShapedRecipeBuilder.shaped(pPackedCategory, pPacked).define('#', pUnpacked).pattern("###").pattern("###").pattern("###").group(pPackedGroup).unlockedBy(getHasName(pUnpacked), has(pUnpacked)).save(pFinishedRecipeConsumer, DungeonsPlus.locate(pPackedName));
	}
	// VANILLA COPY END

	private static String find(String key)
	{
		return DungeonsPlus.locate(key).toString();
	}
}
